
#include "usbd_cdc_if.h"

// Implement putchar and write syscalls so printf can be used
int __io_putchar(int ch) {
  CDC_Transmit_FS((uint8_t *) &ch, 1);

  return ch;
}

int _write(int file, char *ptr, int len) {
  UNUSED(file);

  CDC_Transmit_FS((uint8_t *) ptr, len);

  return len;
}
