# Setting up Serial over USB

This document describes the process of setting up a serial connection over the bottom micro usb port of the stm32f411 discovery board.


## Enabling USB CDC (Virtial COM Port):

We will be using USB CDC which will simulate a serial port, with all the bells and whistles like automatic
baud rate detection!

Connectivity -> USB_OTG_FS set to "Device Only"
Middleware -> USB_DEVICE set to "Communication Class Device (Virtual Port Com)"

STM32 is now able to send data over a usb serial connection

```c
  CDC_Transmit_FS((uint8_t *) data, len);
```

### Setting Up printf:

To be able to use printf, the following syscalls need to be implemented:

`__io__putchar(int)` and `_write(...)`.

See [usb_uart_setup.c](https://gitlab.com/k1ngjulien/nfcreader/-/blob/084e489ebeceefcc0f9b2a29e6b8a28d6cb72563/Core/Src/usb_uart_setup.c) 
for Implementations.


Note:

According to this guide, `__io_putchar` should be the only method required to be implemented but for some reason it only works with write.
https://community.st.com/s/article/how-to-redirect-the-printf-function-to-a-uart-for-debug-messages

I'm leaving putchar implemented in case some library functions want to use that instead of write.

### see serial output on computer

to see the output of the usb serial interface, connect a micro usb cable to the bottom usb port on the stm32 board.

use a terminal emulator like `PuTTY` to connect to the interface (`/dev/ttyACM0` on linux and probably something like `COM3` on windows)

you may use any baud rate you like, the interface automatically adjusts its speed. 115200Bit/s seems to work fine.

