
# Adapterboard

In order to facilitate a reliable connection between the STM32 and the NFC-Reader board, an adapter board was build.

This exposes a male header with the same pinout as the I2C connection on the NFC-Board.

It also incorperates two 10k Pull-Up-Resistors on the SDA and SCL lines of the I2C Interface.

![](adapterboard_schematic.png)

## Setup

![](setup.jpg)

## Front

![](adapterboard.jpg)

## Back

![](adapterboard_back.jpg)
