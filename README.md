# NFCReader

## Pinbelegung:

verbindung mit I2C1, pins remapped

PB09: SDA + internal pull Up

PB08: SCL + internal pull up

GND -> GND
VCC -> 3.3V


## Documents:

## PN532 LIB!! WhOOOO

https://github.com/soonuse/pn532-lib

## Datenblatt von NXP

https://www.nxp.com/docs/en/nxp/data-sheets/PN532_C1.pdf

## User Manual! erklärt das frame based protokoll

https://www.nxp.com/docs/en/user-guide/141520.pdf

## Adafruit modul und lib

Adafruit hat ein Modul basierend auf dem gleichen Chip und eine Library dazu:
https://learn.adafruit.com/adafruit-pn532-rfid-nfc/downloads

https://github.com/adafruit/Adafruit-PN532


# Standards and Technologies

## NFCIP-1

NFC Protocol Definition.

We could probably get this thing talking to a smartphone.

This thing is open souce! <3

https://www.ecma-international.org/wp-content/uploads/ECMA-340_3rd_edition_june_2013.pdf

## Mifare

NXP RFID Chip technology "based on [...] various levels of the ISO/IEC 14443 Type A 13.56 MHz contactless smart card standard."

https://en.wikipedia.org/wiki/Mifare

## ISO/IEC14443

this is not open souce :( 

https://www.iso.org/standard/73599.html


## libnfc:

library to talk to nfc card readers on linux.

https://github.com/nfc-tools/libnfc


## NFC Tools android app

allows reading/writing of many cards


https://play.google.com/store/apps/details?id=com.wakdev.wdnfc


### fh campus card

interestingly, the fh campus card is only partially readable with this app



# Ziel:

Lesen von Karten und Anzeige auf 7-Seg-Display


# Projektdoku

> Ausschließlich I2C verwenden + HAL

Präsentation der Ergebnisse: 6. Juni
Projektdokumentation
- Ziele
- technische Daten
- Aufbau
- Test(s)
	- Foto/Video
- Ergebnisse
- Code online (.zip)
- Treiber-Libraries erlaubt wenn richtig Zitiert/Quelle angegeben

Deckblatt mit Titel und LVA-Nummer


## Tips

I2C-HAL Treiber
Achtung bei der Angabe der I2C-Adresse (R/W-Bit)



